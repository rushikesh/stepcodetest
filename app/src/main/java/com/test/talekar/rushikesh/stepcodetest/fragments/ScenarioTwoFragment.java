package com.test.talekar.rushikesh.stepcodetest.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.test.talekar.rushikesh.model.LocationData;
import com.test.talekar.rushikesh.presenter.LocationPresenter;
import com.test.talekar.rushikesh.presenter.contracts.LocationContract;
import com.test.talekar.rushikesh.stepcodetest.R;
import com.test.talekar.rushikesh.stepcodetest.activities.HomeActivity;
import com.test.talekar.rushikesh.stepcodetest.util.CollectionUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link ScenarioTwoFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 * @author Rushikesh_Talekar
 */
public class ScenarioTwoFragment extends AppFragment implements LocationContract {
  public static final String TAG = ScenarioTwoFragment.class.getSimpleName();
  private Spinner spLocation;
  private List<LocationData> locationDataList;
  private TextView tvlocation;
  private Button btnnavigate;
  private int selectedItem;
  private OnNavigateClicked listner;
  public static final String ARG_DATA = "arg_data";

  public ScenarioTwoFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment ScenarioTwoFragment.
   */
  public static ScenarioTwoFragment newInstance() {
    ScenarioTwoFragment fragment = new ScenarioTwoFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View v = inflater.inflate(R.layout.fragment_scenario_two, container, false);
    initUI(v);
    setListners();

    if (null != savedInstanceState) {
      locationDataList = savedInstanceState.getParcelableArrayList(ARG_DATA);
      updateUI(locationDataList);
    } else {
      getLocationData();
    }
    return v;
  }

  /**
   * This method will register all listners
   */
  private void setListners() {
    spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        setDataToView(i);
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    });

    btnnavigate.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        navigateToMaps();
      }
    });
  }

  /**
   * Launch map to show selected location
   */
  private void navigateToMaps() {
    LocationData locationData = locationDataList.get(selectedItem);
    String uri = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f", locationData.getLocation()
        .getLatitude(), locationData.getLocation().getLongitude(), locationData.getLocation()
        .getLatitude(), locationData.getLocation().getLongitude());
    Log.e(TAG, "URI " + uri);
    listner.onnavigateClicked(uri);
  }

  /**
   * Map data from API to respective view
   *
   * @param position - selected item position
   */
  private void setDataToView(int position) {
    selectedItem = position;
    String data = "";
    LocationData locationData = locationDataList.get(position);
    if (null != locationData.getFromcentral()) {
      if (!TextUtils.isEmpty(locationData.getFromcentral().getCar())) {
        data = getString(R.string.car) + locationData.getFromcentral().getCar();
      }
      if (!TextUtils.isEmpty(locationData.getFromcentral().getTrain())) {
        data += getString(R.string.train) + locationData.getFromcentral().getTrain();
      }
    }
    tvlocation.setText(data);
  }

  private void initUI(View v) {
    spLocation = v.findViewById(R.id.sp_location);
    tvlocation = v.findViewById(R.id.tv_location);
    btnnavigate = v.findViewById(R.id.btn_navigate);
  }

  private void getLocationData() {
    LocationContract.UserActionListner locationPresenter = new LocationPresenter(this);
    showLoading(true);
    locationPresenter.getLocationData();
  }

  @Override
  public void onGetLocationSuccess(List<LocationData> locationDataList) {
    showLoading(false);
    Log.e(TAG, "" + locationDataList.size());
    updateUI(locationDataList);
  }

  /**
   * Map data from API to respective view
   *
   * @param locationDataList - data from API
   */
  private void updateUI(List<LocationData> locationDataList) {
    if (CollectionUtil.isEmpty(locationDataList)) {
      Toast.makeText(getContext(), "No data available..", Toast.LENGTH_SHORT).show();
      return;
    }
    this.locationDataList = locationDataList;
    spLocation.setVisibility(View.VISIBLE);
    String[] nameLocations = new String[locationDataList.size()];
    for (int i = 0; i < nameLocations.length; i++) {
      nameLocations[i] = locationDataList.get(i).getName();
    }
    ArrayAdapter<String> carrierAdapter = new
        ArrayAdapter<>(getActivity(), R.layout.tv_spinner, nameLocations);
    carrierAdapter.setDropDownViewResource(R.layout.tv_spinner);
    spLocation.setAdapter(carrierAdapter);
  }

  @Override
  public void onGetLocationFailure() {
    showLoading(false);
    Toast.makeText(getContext(), "Error while fetching data..", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);

    // This makes sure that the container activity has implemented
    // the callback interface. If not, it throws an exception
    try {
      if (context instanceof Activity) {
        listner = (HomeActivity) context;
      }
    } catch (ClassCastException e) {
      throw new ClassCastException(context.toString()
          + " must implement OnHeadlineSelectedListener");
    }
  }

  @Override
  public void onDetach() {
    listner = null;
    super.onDetach();
  }

  // Container Activity must implement this interface
  public interface OnNavigateClicked {
    void onnavigateClicked(String uri);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putParcelableArrayList(ARG_DATA, (ArrayList<? extends Parcelable>) locationDataList);
  }
}
