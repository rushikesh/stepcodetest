package com.test.talekar.rushikesh.stepcodetest.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.test.talekar.rushikesh.stepcodetest.R;
import com.test.talekar.rushikesh.stepcodetest.customviews.CustomDrawerLayout;
import com.test.talekar.rushikesh.stepcodetest.fragments.ScenarioOneFragment;
import com.test.talekar.rushikesh.stepcodetest.fragments.ScenarioTwoFragment;
import com.test.talekar.rushikesh.stepcodetest.util.Constants;

/**
 * This is a launcher activity.
 *
 * @author Rushikesh_Talekar
 */
public class HomeActivity extends AppActivity implements View.OnClickListener,
    CustomDrawerLayout.DrawerLayoutCallBack, ScenarioTwoFragment.OnNavigateClicked {

  private CustomDrawerLayout drawerLayout;
  private int selectedScenario = 1;
  public static final String ARG_SELECTED_SCENARIO = "arg_selected_scenario";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    initUI();
    setListners();

    if (null != savedInstanceState) {
      selectedScenario = savedInstanceState.getInt(ARG_SELECTED_SCENARIO);
      Log.e("TAG", "Selected Scenario" + selectedScenario);
      if (selectedScenario == 1) {
        scenarioOneSelected();
      } else {
        scenarioTwoSelected();
      }
    } else {
      //Load first fragment
      scenarioOneSelected();
    }
  }

  /**
   * This method will initialize all listners.
   */
  private void setListners() {
    ivHamburger.setOnClickListener(this);
  }

  /**
   * This method will initialize views as required.
   */
  private void initUI() {
    //Initialize toolbar
    Toolbar toolbar = findViewById(R.id.toolbar);
    initializeToolbar(toolbar);

    //Initialise drawer
    drawerLayout = findViewById(R.id.drawer_layout);
    drawerLayout.init();
    drawerLayout.setDrawerLayoutCallBack(this);
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.iv_hamburger:
        openDrawer();
        break;
    }
  }

  /**
   * This method opens the drawer layout for the activity.
   */
  private void openDrawer() {
    drawerLayout.openDrawer(GravityCompat.END);
  }

  @Override
  protected void onDestroy() {
    if (null != drawerLayout) {
      drawerLayout.removeDrawerLayoutCallBack();
    }
    super.onDestroy();
  }

  @Override
  public void scenarioOneSelected() {
    selectedScenario = 1;
    getSupportFragmentManager()
        .beginTransaction()
        .replace(R.id.container, ScenarioOneFragment.newInstance(),
            Constants.NAVIGATION_TAG_FRAGMENT_SCENARIO_1)
        .commit();
  }

  @Override
  public void scenarioTwoSelected() {
    selectedScenario = 2;
    getSupportFragmentManager()
        .beginTransaction()
        .replace(R.id.container, ScenarioTwoFragment.newInstance(),
            Constants.NAVIGATION_TAG_FRAGMENT_SCENARIO_2)
        .commit();
  }

  @Override
  public void onnavigateClicked(String uri) {
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
    startActivity(intent);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    outState.putInt(ARG_SELECTED_SCENARIO, selectedScenario);
    super.onSaveInstanceState(outState);
  }
}
