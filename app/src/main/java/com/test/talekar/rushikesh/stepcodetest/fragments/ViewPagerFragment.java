package com.test.talekar.rushikesh.stepcodetest.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.test.talekar.rushikesh.stepcodetest.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewPagerFragment extends Fragment {

  private static final String ARG_POSITION = "arg_position";

  public ViewPagerFragment() {
    // Required empty public constructor
  }

  public static ViewPagerFragment newInstance(int position) {
    ViewPagerFragment viewPagerFragment = new ViewPagerFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_POSITION, position);
    viewPagerFragment.setArguments(args);
    return viewPagerFragment;
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View v = inflater.inflate(R.layout.fragment_view_pager, container, false);
    initUI(v);
    return v;
  }

  /**
   * This method is used to initialize UI
   */
  int position = 0;

  private void initUI(View v) {
    if (null != getArguments()) {
      position = getArguments().getInt(ARG_POSITION);
    }
    FrameLayout viewPagerItem = v.findViewById(R.id.fl_viewpager_item);
    viewPagerItem.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Toast.makeText(getContext(), "Position: " + position, Toast.LENGTH_SHORT).show();
      }
    });

    TextView tvTitle = v.findViewById(R.id.tv_viewpager_item);
    tvTitle.setText("Fragment: " + position);
  }

}
