package com.test.talekar.rushikesh.stepcodetest.callbacks;

/**
 * This interface will have a set of rules for showing progress dialog.
 *
 * @author Rushikesh_Talekar
 */

public interface ProgressLoaderView {

  /**
   * Method to show and hide progress dialog
   *
   * @param show
   */
  void showLoading(boolean show);

  /**
   * Method to show and hide progress dialog with message
   *
   * @param show - true to show otherwise false
   * @param msg  - message to be displayed
   */
  void showLoading(boolean show, String msg);
}
