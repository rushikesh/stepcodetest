package com.test.talekar.rushikesh.stepcodetest.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.test.talekar.rushikesh.stepcodetest.R;

/**
 * This will be a parent Activity of all other activities.
 * All common code among activities will go here.
 *
 * @author Rushikesh_Talekar
 */
public class AppActivity extends AppCompatActivity {


  protected ImageView ivHamburger;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_app);
  }


  /**
   * This method will initialise common features of toolbar
   *
   * @param toolbar - Toolbar object to customize
   */
  public void initializeToolbar(Toolbar toolbar) {
    setSupportActionBar(toolbar);
    if (null != getSupportActionBar()) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
      ivHamburger = findViewById(R.id.iv_hamburger);
    }
  }
}
