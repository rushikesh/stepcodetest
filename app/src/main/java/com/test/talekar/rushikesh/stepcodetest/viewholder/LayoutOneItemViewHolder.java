package com.test.talekar.rushikesh.stepcodetest.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.test.talekar.rushikesh.stepcodetest.R;

/**
 * View holder to get recycler view item.
 */

public class LayoutOneItemViewHolder extends RecyclerView.ViewHolder {

  private final TextView tvIdentifier;

  public TextView getTvIdentifier() {
    return tvIdentifier;
  }

  public LayoutOneItemViewHolder(View itemView) {
    super(itemView);
    tvIdentifier = itemView.findViewById(R.id.tv_list_item);
  }


}
