package com.test.talekar.rushikesh.stepcodetest.fragments;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.test.talekar.rushikesh.stepcodetest.R;
import com.test.talekar.rushikesh.stepcodetest.adapters.LayoutOneItemAdapter;
import com.test.talekar.rushikesh.stepcodetest.adapters.ViewPagerAdapter;
import com.test.talekar.rushikesh.stepcodetest.util.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link ScenarioOneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScenarioOneFragment extends AppFragment
    implements LayoutOneItemAdapter.OnListItemSelected, View.OnClickListener {


  private TextView tvLayout4;
  private LinearLayout llLayout5;

  public ScenarioOneFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment ScenarioOneFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static ScenarioOneFragment newInstance() {
    ScenarioOneFragment fragment = new ScenarioOneFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View v = inflater.inflate(R.layout.fragment_scenario_one, container, false);
    initUI(v);
    return v;
  }

  private void initUI(View v) {
    // Initialize Layout 1
    final RecyclerView rvVehicleInfoList = v.findViewById(R.id.rv_layout_1);
    DividerItemDecoration customDivider =
        new DividerItemDecoration(ContextCompat.getDrawable(getContext(),
            R.drawable.line_divider), true, true);
    rvVehicleInfoList.addItemDecoration(customDivider);
    LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getContext(),
        LinearLayoutManager.HORIZONTAL, false);
    rvVehicleInfoList.setLayoutManager(horizontalLayoutManagaer);
    LayoutOneItemAdapter layoutOneItemAdapter = new LayoutOneItemAdapter(getContext());
    layoutOneItemAdapter.setOnItemSelectedListner(this);
    rvVehicleInfoList.setAdapter(layoutOneItemAdapter);

    // Initialize Layout 2
    ViewPager viewPager = v.findViewById(R.id.pager);
    ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
    final List<ViewPagerFragment> addedFragment = new ArrayList<>();
    for (int i = 0; i < 4; i++) {
      ViewPagerFragment viewPagerFragment = ViewPagerFragment.newInstance(i);
      addedFragment.add(i, viewPagerFragment);
      pagerAdapter.addFragment(viewPagerFragment);
    }
    viewPager.setOffscreenPageLimit(3);
    viewPager.setAdapter(pagerAdapter);
    CircleIndicator indicator = v.findViewById(R.id.indicator);
    indicator.setViewPager(viewPager);

    // Initialize Layout 4
    tvLayout4 = v.findViewById(R.id.tv_layout_4);

    //Initialize layout 5
    llLayout5 = v.findViewById(R.id.ll_layout_5);
    Button btnRed = v.findViewById(R.id.btn_red);
    Button btnGreen = v.findViewById(R.id.btn_green);
    Button btnBlue = v.findViewById(R.id.btn_blue);

    btnRed.setOnClickListener(this);
    btnGreen.setOnClickListener(this);
    btnBlue.setOnClickListener(this);
  }

  @Override
  public void listItemSelected(String title, int position) {
    tvLayout4.setText(title);
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.btn_red:
        setParentColor(android.R.color.holo_red_dark);
        break;
      case R.id.btn_green:
        setParentColor(android.R.color.holo_green_dark);
        break;
      case R.id.btn_blue:
        setParentColor(android.R.color.holo_blue_dark);
        break;
    }
  }

  private void setParentColor(int color) {
    llLayout5.setBackgroundColor(ActivityCompat.getColor(getContext(), color));
  }
}
