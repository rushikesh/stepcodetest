package com.test.talekar.rushikesh.stepcodetest.customviews;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.test.talekar.rushikesh.stepcodetest.R;

/**
 * This is a custom drawer layout
 *
 * @author Rushikesh_Talekar
 */

public class CustomDrawerLayout extends DrawerLayout {
  private static final int SCENARIO1 = 0;
  private static final int SCENARIO2 = 1;
  private ListView drawerList;
  private DrawerLayoutCallBack drawerLayoutCallBack;
  private RelativeLayout drawerContainer;

  public CustomDrawerLayout(Context context) {
    super(context);
  }

  public CustomDrawerLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public CustomDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  /**
   * This method will initialise all parameters
   */
  public void init() {
    drawerContainer = findViewById(R.id.right_drawer);
    drawerList = findViewById(R.id.drawer_items_list);
    String[] navigationDrawerItems =
        getResources().getStringArray(R.array.navigation_drawer_items_array);
    ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.drawer_item,
        navigationDrawerItems);
    drawerList.setAdapter(adapter);
    drawerList.setOnItemClickListener(new DrawerItemClickListener());
    addDrawerListener(new DrawerListener() {
      @Override
      public void onDrawerSlide(View drawerView, float slideOffset) {

      }

      @Override
      public void onDrawerOpened(View drawerView) {
      }

      @Override
      public void onDrawerClosed(View drawerView) {
      }

      @Override
      public void onDrawerStateChanged(int newState) {

      }
    });

    drawerContainer.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        /*
         * Click listener to capture click events on drawer preventing user from interacting
         * with UI components behind the drawer
         */
      }
    });
  }

  /**
   * This is a listner to catch list item clicks
   */
  private class DrawerItemClickListener implements ListView.OnItemClickListener {

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
      selectItem(position);
    }

  }

  /**
   * This method will initialise a listner
   *
   * @param drawerLayoutCallBack
   */
  public void setDrawerLayoutCallBack(DrawerLayoutCallBack drawerLayoutCallBack) {
    this.drawerLayoutCallBack = drawerLayoutCallBack;
  }

  /**
   * Method to remove callback
   */
  public void removeDrawerLayoutCallBack() {
    this.drawerLayoutCallBack = null;
  }

  private void selectItem(int position) {
    drawerList.setItemChecked(position, true);
    drawerList.setSelection(position);
    closeDrawer(drawerContainer);
    switch (position) {
      case SCENARIO1:
        drawerLayoutCallBack.scenarioOneSelected();
        break;
      case SCENARIO2:
        drawerLayoutCallBack.scenarioTwoSelected();
        break;
      default:
        break;
    }
  }


  /**
   * This class is used to interact with activity
   */
  public interface DrawerLayoutCallBack {
    /**
     * This method will get called when uset clicks scenario 1
     */
    void scenarioOneSelected();

    /**
     * This method will get called when uset clicks scenario 2
     */
    void scenarioTwoSelected();

  }
}
