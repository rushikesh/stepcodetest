package com.test.talekar.rushikesh.stepcodetest;

import android.app.Application;

import com.test.talekar.rushikesh.commons.SessionManager;

/**
 * Created by Rushikesh_Talekar on 13-12-2017.
 */

public class StepCodeTestApplication extends Application {

  @Override
  public void onCreate() {
    super.onCreate();

    SessionManager.init(BuildConfig.BASE_URL);
  }
}
