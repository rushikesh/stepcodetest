package com.test.talekar.rushikesh.stepcodetest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.talekar.rushikesh.stepcodetest.R;
import com.test.talekar.rushikesh.stepcodetest.viewholder.LayoutOneItemViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter to show 5 different items in layout one.
 *
 * @author Rushikesh_Talekar
 */

public class LayoutOneItemAdapter
    extends RecyclerView.Adapter<LayoutOneItemViewHolder> {
  private final List<String> itemList = new ArrayList<>();
  private OnListItemSelected onListItemSelected;

  public LayoutOneItemAdapter(Context context) {
    itemList.add(context.getString(R.string.item_1));
    itemList.add(context.getString(R.string.item_2));
    itemList.add(context.getString(R.string.item_3));
    itemList.add(context.getString(R.string.item_4));
    itemList.add(context.getString(R.string.item_5));
  }

  @Override
  public LayoutOneItemViewHolder onCreateViewHolder(ViewGroup parent,
                                                    int viewType) {
    final View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.textview_layout, parent, false);

    return new LayoutOneItemViewHolder(view);
  }

  public void setOnItemSelectedListner(OnListItemSelected onListItemSelected) {
    this.onListItemSelected = onListItemSelected;
  }

  @Override
  public void onBindViewHolder(LayoutOneItemViewHolder holder, final int position) {
    holder.getTvIdentifier().setText(itemList.get(position));
    holder.getTvIdentifier().setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onListItemSelected.listItemSelected(itemList.get(position), position);
      }
    });
  }

  @Override
  public int getItemCount() {
    return itemList == null ? 0 : itemList.size();
  }

  /**
   * Callback to notify UI that list item is selected.
   */
  public interface OnListItemSelected {
    /**
     * Method to provide selected list item details to UI.
     *
     * @param title    - title of selected item
     * @param position - position of selected item
     */
    void listItemSelected(String title, int position);
  }
}
