package com.test.talekar.rushikesh.stepcodetest.util;

/**
 * This class will store all constants used in application.
 */

public class Constants {
  //Navigation Constants
  public static final String NAVIGATION_TAG_FRAGMENT_SCENARIO_1 =
      "NAVIGATION_TAG_FRAGMENT_SCENARIO_1";
  public static final String NAVIGATION_TAG_FRAGMENT_SCENARIO_2 =
      "NAVIGATION_TAG_FRAGMENT_SCENARIO_2";
}
