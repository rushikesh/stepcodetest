package com.test.talekar.rushikesh.presenter;

import com.test.talekar.rushikesh.LocationServiceImpl;
import com.test.talekar.rushikesh.contracts.LocationServiceContract;
import com.test.talekar.rushikesh.model.LocationData;
import com.test.talekar.rushikesh.presenter.contracts.LocationContract;

import java.util.List;

/**
 * Created by Rushikesh_Talekar on 13-12-2017.
 */

public class LocationPresenter implements LocationContract.UserActionListner,
    LocationServiceContract.Callback {
  LocationContract viewContract;

  public LocationPresenter(LocationContract viewContract) {
    this.viewContract = viewContract;
  }

  @Override
  public void getLocationData() {
    LocationServiceContract locationServiceContract = new LocationServiceImpl(this);
    locationServiceContract.getLocationData();
  }

  @Override
  public void onGetLocationSuccess(List<LocationData> locationDataList) {
    if (null != viewContract) {
      viewContract.onGetLocationSuccess(locationDataList);
    }
  }

  @Override
  public void onGetLocationFailure() {
    if (null != viewContract) {
      viewContract.onGetLocationFailure();
    }
  }
}
