package com.test.talekar.rushikesh.presenter.contracts;

import com.test.talekar.rushikesh.model.LocationData;

import java.util.List;

/**
 * This interface will have all contract method between presenter and UI.
 * <p>
 * Created by Rushikesh_Talekar on 13-12-2017.
 */

public interface LocationContract {
  /**
   * This method will notify user that API request is successful.
   *
   * @param locationDataList - Locationn datareturned by API
   */
  void onGetLocationSuccess(List<LocationData> locationDataList);

  /**
   * This method will notify user that API request is failed.
   */
  void onGetLocationFailure();

  /**
   * This class is to initiate call from presenter.
   *
   * @author Rushikesh_Talekar
   */
  interface UserActionListner {

    void getLocationData();
  }
}
