package com.test.talekar.rushikesh.api;

import com.test.talekar.rushikesh.commons.Constants;
import com.test.talekar.rushikesh.model.LocationData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;

/**
 * Created by Rushikesh_Talekar on 13-12-2017.
 */

public interface GetLocationAPI {
  /**
   * Get location data.
   *
   */
  @GET(Constants.URL_GET_LOCATION_DATA)
  Call<List<LocationData>> getLocationData();
}
