package com.test.talekar.rushikesh.contracts;

import com.test.talekar.rushikesh.model.LocationData;

import java.util.List;

/**
 * Created by Rushikesh_Talekar on 13-12-2017.
 */

public interface LocationServiceContract {
  /**
   * This method will initiate atual service call to get data.
   */
  void getLocationData();

  interface Callback {
    /**
     * This method will notify user that API request is successful.
     *
     * @param locationDataList - Locationn datareturned by API
     */
    void onGetLocationSuccess(List<LocationData> locationDataList);

    /**
     * This method will notify user that API request is failed.
     */
    void onGetLocationFailure();
  }
}
