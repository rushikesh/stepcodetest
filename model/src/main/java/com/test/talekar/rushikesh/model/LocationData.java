package com.test.talekar.rushikesh.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rushikesh_Talekar on 13-12-2017.
 */

public class LocationData implements Parcelable {
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("fromcentral")
  @Expose
  private Fromcentral fromcentral;
  @SerializedName("location")
  @Expose
  private Location location;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Fromcentral getFromcentral() {
    return fromcentral;
  }

  public void setFromcentral(Fromcentral fromcentral) {
    this.fromcentral = fromcentral;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  @Override
  public String toString() {
    return "LocationData{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", fromcentral=" + fromcentral +
        ", location=" + location +
        '}';
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.id);
    dest.writeString(this.name);
    dest.writeParcelable(this.fromcentral, flags);
    dest.writeParcelable(this.location, flags);
  }

  public LocationData() {
  }

  protected LocationData(Parcel in) {
    this.id = (Integer) in.readValue(Integer.class.getClassLoader());
    this.name = in.readString();
    this.fromcentral = in.readParcelable(Fromcentral.class.getClassLoader());
    this.location = in.readParcelable(Location.class.getClassLoader());
  }

  public static final Parcelable.Creator<LocationData> CREATOR =
      new Parcelable.Creator<LocationData>() {
        @Override
        public LocationData createFromParcel(Parcel source) {
          return new LocationData(source);
        }

        @Override
        public LocationData[] newArray(int size) {
          return new LocationData[size];
        }
      };
}
