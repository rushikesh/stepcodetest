package com.test.talekar.rushikesh;

import com.test.talekar.rushikesh.api.GetLocationAPI;
import com.test.talekar.rushikesh.commons.SessionManager;
import com.test.talekar.rushikesh.contracts.LocationServiceContract;
import com.test.talekar.rushikesh.model.LocationData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rushikesh_Talekar on 13-12-2017.
 */

public class LocationServiceImpl implements LocationServiceContract,
    Callback<List<LocationData>> {
  LocationServiceContract.Callback callback;

  public LocationServiceImpl(
      LocationServiceContract.Callback callback) {
    this.callback = callback;
  }

  @Override
  public void getLocationData() {
    try {
      Call<List<LocationData>> call = SessionManager.getSessionManager()
          .createRestAdapter().create(GetLocationAPI.class).getLocationData();
      call.enqueue(this);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onResponse(Call<List<LocationData>> call, Response<List<LocationData>> response) {
    if (response.isSuccessful()) {
      callback.onGetLocationSuccess(response.body());
    }
  }

  @Override
  public void onFailure(Call<List<LocationData>> call, Throwable throwable) {
    callback.onGetLocationFailure();
  }
}
