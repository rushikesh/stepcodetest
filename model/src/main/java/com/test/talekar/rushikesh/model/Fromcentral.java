package com.test.talekar.rushikesh.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rushikesh_Talekar on 13-12-2017.
 */

public class Fromcentral implements Parcelable {

  @SerializedName("car")
  @Expose
  private String car;
  @SerializedName("train")
  @Expose
  private String train;

  public String getCar() {
    return car;
  }

  public void setCar(String car) {
    this.car = car;
  }

  public String getTrain() {
    return train;
  }

  public void setTrain(String train) {
    this.train = train;
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.car);
    dest.writeString(this.train);
  }

  public Fromcentral() {
  }

  protected Fromcentral(Parcel in) {
    this.car = in.readString();
    this.train = in.readString();
  }

  public static final Parcelable.Creator<Fromcentral> CREATOR =
      new Parcelable.Creator<Fromcentral>() {
        @Override
        public Fromcentral createFromParcel(Parcel source) {
          return new Fromcentral(source);
        }

        @Override
        public Fromcentral[] newArray(int size) {
          return new Fromcentral[size];
        }
      };
}
