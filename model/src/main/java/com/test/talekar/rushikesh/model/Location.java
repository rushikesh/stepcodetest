package com.test.talekar.rushikesh.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rushikesh_Talekar on 13-12-2017.
 */


public class Location implements Parcelable {

  @SerializedName("latitude")
  @Expose
  private Double latitude;
  @SerializedName("longitude")
  @Expose
  private Double longitude;

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.latitude);
    dest.writeValue(this.longitude);
  }

  public Location() {
  }

  protected Location(Parcel in) {
    this.latitude = (Double) in.readValue(Double.class.getClassLoader());
    this.longitude = (Double) in.readValue(Double.class.getClassLoader());
  }

  public static final Parcelable.Creator<Location> CREATOR = new Parcelable.Creator<Location>() {
    @Override
    public Location createFromParcel(Parcel source) {
      return new Location(source);
    }

    @Override
    public Location[] newArray(int size) {
      return new Location[size];
    }
  };
}
